# Provides helper methods for generating TwiML
module Webhookable
  extend ActiveSupport::Concern

  def set_header
    response.headers['Content-Type'] = 'text/xml'
  end
end
